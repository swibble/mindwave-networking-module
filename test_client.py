#!/usr/bin/python

## Author:  Luke Caldwell
## Org: Duke University Speculative Sensation Lab
## Website: http://s-1lab.org
## License: Creative Commons BY-SA 3.0
##          http://creativecommons.org/licenses/by-sa/3.0/

from __future__ import print_function
from argparse import ArgumentParser
import socket
import random
import json
import time
import threading

parser = ArgumentParser()
parser.add_argument('-p', '--port', dest="port", action="store", type=int,
                    default=9999, help="output port")
parser.add_argument('-H', '--host', dest="host", action="store",
                    default="127.0.0.1", help="host address")
parser.add_argument('-v', '-verbose', dest="verbose", action="store_true",
                    default=False, help="print out more information")
parser.add_argument('-t', '--threads', dest="threads", action="store", type=int,
                    default=4, help="specify number of threads")
args = parser.parse_args()


def printer(*s):
    if args.verbose:
        print(*s)


def main(host, port):
    server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    client_id = random.randint(1000, 9999)
    printer(client_id)
    while True:
        out = {'attention': random.randint(1, 100),
               'meditation': random.randint(1, 100),
               'timestamp': time.time(),
               'id': client_id}
        printer(out)
        server.sendto(json.dumps(out), (host, port))
        time.sleep(1)

print("Running pyndwave test client...")
for n in range(0, args.threads):
    t = threading.Thread(target=main, args=(args.host, args.port))
    t.start()
    time.sleep(.2)

