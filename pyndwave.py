#!/usr/bin/python

## Author:  Luke Caldwell
## Org: Duke University Speculative Sensation Lab
## Website: http://s-1lab.org
## License: Creative Commons BY-SA 3.0
##          http://creativecommons.org/licenses/by-sa/3.0/

"""
Networking module for Neurosky's MindWave EEG headset.

Allows for streaming headset data to server where the 
values are averaged. Number of potential connections
is unlimited.
"""

from __future__ import print_function
from argparse import ArgumentParser
import sys
import SocketServer
import socket
import json
import random
import time
import re


###########################################################
#   CLI Parser
###########################################################

parser = ArgumentParser()
parser.add_argument('-s', '--server', dest="server", action="store_true",
                    default=False, help="Activates server mode")
parser.add_argument('-c', '--client', dest="server", action="store_false",
                    help="Activates client mode")
parser.add_argument('-H', '--host', dest="host", action="store",
                    default="127.0.0.1", 
                    help="""host address for relaying information (in client 
                        mode), host address for running server (in server 
                        mode)""")
parser.add_argument('-p', '--port', dest="port", action="store", type=int,
                    default=9999, help="""Specify port for server or client. 
                                        Integers only.""")
parser.add_argument('-v', '--verbose', dest="debug", action="store_true",
                    default=False, help="""Prints additional information to 
                                        console.""")
parser.add_argument('--sync', dest="sync", action="store", type=int,
                    default=5, help="Set the number of seconds for latency.")
parser.add_argument('--osc', dest="osc", action="store_true",
                    default=False, help="Sets server output to OSC.")
parser.add_argument( '-ho','--host_out', dest="host_out", action="store",
                    default='localhost', help="Sets host address for server output.")
parser.add_argument( '-po', '--port_out', dest="port_out", action="store", type=int,
                    default='1234', help="Sets port for server output.")
args = parser.parse_args()

if args.server and args.osc:
    sys.path.append('./libs')
    try:
        import OSC
    except ImportError:
        raise Exception('Please add pyOSC to the libs folder...')
elif args.server:
    import requests

if not re.search('\d+\.\d+\.\d+\.\d+', args.host):
    raise Exception('Please specify an ip address such as 127.0.0.1.')

if args.port < 1024:
    raise Exception('Please choose a higher port number, i.e. > 1023.')

CONNECTION = (args.host, args.port)
EXT_CONNECTION = (args.host_out, args.port_out)

###########################################################
#   Debug
###########################################################
def logger(*s):
    if args.debug:
        print(*s)

###########################################################
#   Server
###########################################################

class MWServer():

    ts = time.time()
    connections = {}
    attention = 0
    meditation = 0
    if args.osc:
        c = OSC.OSCClient()
        c.connect(EXT_CONNECTION)
    else:
        server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    def parse_data(self, data):
        """
        Parses json input and adds to connections dict.

        Each connection has unique identifier and timestamp
        of last update.

        Recalculates average based on new data.

        Sends data out to localhost to pass off to other 
        application.
        """
        d = json.loads(data)
        _id = d['id']
        if 'attention' in d:
            self.connections[_id] = (int(d['attention']), int(d['meditation']), float(d['timestamp']))
            self.set_average()

            #   throttle connections to send output only once a second
            if time.time() - self.ts >= 1:
                self.send_output()
                self.ts = time.time()
        elif 'blinkStrength' in d:
            blink = int(d['blinkStrength'])
            logger(blink)
            self.send_blink(blink)

    def set_average(self):
        self.attention = self.avg([n[0] for n in self.connections.values()])
        self.meditation = self.avg([n[1] for n in self.connections.values()])

        #   clear connections that have over 5 sec delay
        #   to remove data of disconnected users
        for k, v in self.connections.items():
            if (time.time() - v[2]) > args.sync:
                del self.connections[k]
        logger(self.connections, self.attention, self.meditation)    

    def avg(self, n):
        """
        Helper function to return average of list
        """
        return sum(n)/len(n)

    def send_blink(self, n):
        if args.osc:
            mesg = OSC.OSCMessage()
            mesg.setAddress('/isadora/3')
            mesg.append(n)
            self.c.send(mesg)

    def send_output(self):
        """
        Pass off information to localhost port for handling
        by different application. Send in JSON dict with 
        'attention' and 'meditation' values.

        Connection is UDP.
        """

        if args.osc:
            bundle = OSC.OSCBundle()
            attnmesg = OSC.OSCMessage()
            attnmesg.setAddress('/isadora/1')
            attnmesg.append(self.attention)
            bundle.append(attnmesg)
            medmesg = OSC.OSCMessage()
            medmesg.setAddress('/isadora/2')
            medmesg.append(self.meditation)
            bundle.append(medmesg)
            self.c.send(bundle)
        else:
            # d = {'attention': self.attention, 'meditation': self.meditation}
            # self.server.sendto(json.dumps(d), EXT_CONNECTION)
            d = {'a': self.attention, 'm': self.meditation}
            requests.get('http://localhost/data', params=d)

class UDPServer(SocketServer.BaseRequestHandler):
    """
    custom socketserver handler class 
    """
    def handle(self):
        global M
        data = self.request[0].strip()
        M.parse_data(data)

def server_main():
    global M
    M = MWServer()
    print('Server is now active on {0} at port {1}...'.format(args.host, 
                                                            args.port))
    print('Press ctrl+c to stop...')
    server = SocketServer.UDPServer(CONNECTION, UDPServer)
    server.serve_forever()

###########################################################
#   Client
###########################################################

class MWClient():
    
    def setup(self):
        """
        Setup TCP socket to ThinkGear Connector
        """
        logger('Connecting to the mindwave headset...')
        self.local = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.local.connect(('127.0.0.1', 13854))
        init_message = '{\"enableRawOutput\": false, \"format\": \"Json\"}\n'
        self.local.sendall(init_message)

    def main_loop(self):
        """
        Gather data from ThinkGear Connector and relay to server.

        Adds timestamp and id for processing serverside.
        """
        server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        client_id = random.randint(1000, 9999)
        while True:
            try:
                new = self.local.recv(512)
                logger(new)
                j = json.loads(new)
                out = j.get('eSense', None)
                if out:
                    out.update({'timestamp': time.time(), 
                                'id': client_id})
                    server.sendto(json.dumps(out), CONNECTION)
                elif 'blinkStrength' in j:
                    blink = j.get('blinkStrength', None)
                    out = {'blinkStrength': blink, 'id': client_id,
                                    'timestamp': time.time()}
                    server.sendto(json.dumps(out), CONNECTION)
            except KeyboardInterrupt:
                print('Stopping client...')
                self.local.close()
                break
            except ValueError as e:
                logger(e)

def client_main():
    global M
    M = MWClient()
    M.setup()
    M.main_loop()


if __name__ == '__main__':
    M = None    #   Doing this down here so it can be global
    if args.server:
        print('Running in server mode...')
        server_main()
    else:
        print('Running in client mode...')
        client_main()

