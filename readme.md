Author:  Luke Caldwell

Duke University Speculative Sensation Lab

Website: [s-1lab.org](http://s-1lab.org)

License: Creative Commons BY-SA 3.0
          http://creativecommons.org/licenses/by-sa/3.0/

This is a networking module for Neurosky's Mindwave headset.

Out of the box, Neurosky headsets only allow for connecting 
one headset per computer. This module reads the data 
streaming from the headset and sends it out to a server 
that allows unlimited connections. The server currently 
averages attention and meditation values for all connected 
users and allows for these values to be passed off to 
another application.

In current form, it is composed of a server instance and
a client instance. Switching between these is done by 
adding the -s or -c flags accordingly.

The server address is specified with the -H flag and the port -p.

When running in server mode, you can specify a sync value 
for keeping information on the server updated correctly.
If server-time - timestamp of the packets is greater than 
the sync value, the values are removed and no longer used 
to calculate the average. That way users can disconnect 
without needing to restart the server.

* -s, --server = server mode
* -c, --client = client mode (default)
* -H, --host = IP address of server (defaults to 'localhost')
* -p, --port = server port (defaults to 9999)
* -v, --verbose = More console output
* --sync = Number of seconds difference before server discards data from a connection (defaults to 5)*
* --osc = output server stream to osc for connection to Isadora
* -ho, --host_out = Set custom address for averaged data to be sent
* -po, --port_out = Set custom port for averaged data to be sent

To run in server mode with OSC connecting to Isadora, run --> 

```
#!bash

python pyndwave.py -s --osc
```


To run in server mode with OSC connecting to a different computer --> 

```
#!bash

python pyndwave.py -s --osc -ho 192.168.1.100 -po 1234
```


To run in client mode forwarding all information to 192.168.1.100 p9999 --> 

```
#!bash

python pyndwave.py -c -H 192.168.1.100 -p 9999
```
